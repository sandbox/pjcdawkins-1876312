<?php
/**
 * @file
 * Drush commands for the Election DrooPHP Integration module.
 */

/**
 * Implements hook_drush_command().
 */
function election_droophp_drush_command() {
  $items = array();
  $items['election-droophp'] = array(
    'description' => 'Counts the results of an election post using DrooPHP. The report is printed to STDOUT.',
    'arguments' => array(
      'post_id' => 'The ID of the election post (required).',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'required-arguments' => 1,
    'aliases' => array('edroophp'),
  );
  return $items;
}

/**
 * Count the results for an election post.
 *
 * @param int $post_id
 *   The ID of the election post.
 * @param string $method
 *   The count method to use.
 */
function drush_election_droophp($post_id, $method = 'wikipedia') {
  // Load the post.
  $post = election_post_load($post_id);
  if (!$post) {
    return drush_set_error(dt('There are no election posts with the ID @id.', array('@id' => $post_id)));
  }
  $election = $post->election;
  // Check that it is sane to count results for this post.
  if (!election_droophp_check_support($election)) {
    return drush_set_error(dt('This election type ("@type") does not support DrooPHP.', array('@type' => $election->type_info['name'])));
  }
  // Check the method type.
  $allowed_methods = _election_droophp_get_methods();
  if (!isset($allowed_methods[$method])) {
    return drush_set_error(dt('The count method "@method" was not found.', array('@method' => $method)));;
  }
  // Run the count.
  echo drush_html_to_text(drupal_render(election_droophp_count($post, $method)));
}
