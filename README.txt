Election DrooPHP Integration
==========================
This module integrates the Election module with the DrooPHP (Python) STV counting
framework.

Dependencies
------------
* Libraries API (http://drupal.org/project/libraries)
* Election (http://drupal.org/project/election)
  * Election Post (election_post)
  * Election Results (election_results)
  * Election Export (election_export)

Author
-----
Patrick Dawkins (drupal.org username: pjcdawkins)
